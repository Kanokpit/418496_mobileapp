//
//  BinaryMinusOperator.m
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "BinaryMinusOperator.h"

@implementation BinaryMinusOperator

- (double)computeWithLeftOperand:(Operand *)leftOperand andRightOperand:(Operand *)rightOperand
{
    return leftOperand.value - rightOperand.value;
}

- (NSString *)description
{
    return @"-";
}

@end
