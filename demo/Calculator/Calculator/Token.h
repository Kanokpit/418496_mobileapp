//
//  Token.h
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    TokenTypeOperand,
    TokenTypeOperator,
    TokenTypeLeftParenthesis,    
    TokenTypeRightParenthesis
} TokenType;

@interface Token : NSObject

@property (nonatomic, assign) TokenType type;

@end
