//
//  RightParenthesisToken.m
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "RightParenthesisToken.h"

@implementation RightParenthesisToken

- (id)init
{
    self = [super init];
    
    if (self) {
        self.type = TokenTypeRightParenthesis;
    }
    
    return self;
}

- (NSString *)description
{
    return @")";
}

@end
