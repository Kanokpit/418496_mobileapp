//
//  BinaryOperator.m
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "BinaryOperator.h"

@implementation BinaryOperator


- (id)init
{
    self = [super init];
    if (self) {
        self.operatorType = OperatorTypeBinary;
    }
    return self;
}

- (double)computeWithLeftOperand:(Operand *)leftOperand andRightOperand:(Operand *)rightOperand
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException 
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)] 
                                 userInfo:nil];    
}

@end
