//
//  BinaryPlusOperator.m
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "PlusOperator.h"

@implementation PlusOperator

- (double)computeWithLeftOperand:(Operand *)leftOperand andRightOperand:(Operand *)rightOperand
{
    return leftOperand.value + rightOperand.value;
}

- (NSString *)description
{
    return @"+";
}

@end
