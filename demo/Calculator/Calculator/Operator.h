//
//  Operator.h
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Token.h"

typedef enum {
    OperatorNotationPlus,
    OperatorNotationMinus,
    OperatorNotationDivide,
    OperatorNotationTime,
    OperatorNotationPower    
} OperatorNotation;

typedef enum {
    OperatorAssociativityLeftToRight,
    OperatorAssociativityRightToLeft
} OperatorAssociativity;

typedef enum {
    OperatorTypeUnary,
    OperatorTypeBinary
} OperatorType;

@interface Operator : Token

@property (nonatomic, assign) OperatorNotation notation;
@property (nonatomic, assign) OperatorAssociativity associativity;
@property (nonatomic, assign) int precedence;
@property (nonatomic, assign) OperatorType operatorType;

- (id)init;

@end
