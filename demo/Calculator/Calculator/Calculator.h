//
//  Calculator.h
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Token.h"

@class Calculator;

@protocol CalculatorDelegate <NSObject>

- (void)calculator:(Calculator *)calculator didFinishComputeWithResult:(double)result;
- (void)calculator:(Calculator *)calculator didFinishComputeWithError:(NSError *)error;
- (void)calculator:(Calculator *)calculator didFinishUpdateStack:(NSArray *)stack;

@end

@interface Calculator : NSObject

@property (nonatomic, weak) id<CalculatorDelegate> delegate;

- (void) pushToken:(Token *)token;
- (void) evaluate;

@end
