//
//  CalculatorViewController.h
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Calculator.h"

@interface CalculatorViewController : UIViewController <CalculatorDelegate>

@property (weak, nonatomic) IBOutlet UILabel *stackLabel;
@property (weak, nonatomic) IBOutlet UILabel *inputLabel;

@end
