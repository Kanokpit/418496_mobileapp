//
//  Operator.m
//  Calculator
//
//  Created by Sutee Sudprasert on 2/17/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Operator.h"

@implementation Operator

@synthesize notation = _notation;
@synthesize precedence = _precedence;
@synthesize associativity = _associativity;
@synthesize operatorType = _operatorType;

- (id) init
{
    self = [super init];
    if (self) {
        self.type = TokenTypeOperator;
    }
    return self;
}

@end
