package th.ac.ku.android.sutee.dotdot.fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

public class DotListAdapter extends BaseAdapter implements ListAdapter {

	Context mContext;

	public DotListAdapter(Context context) {
		mContext = context;
	}

	public static interface DotDataSource {
		int size();
		Dot dot(int position);
	}

	DotDataSource dataSource;

	public void setDotDataSource(DotDataSource dotDataSource) {
		this.dataSource = dotDataSource;
	}

	@Override
	public int getCount() {
		if (dataSource == null) {
			return 0;
		}
		return dataSource.size();
	}

	@Override
	public Object getItem(int position) {
		if (dataSource == null) {
			return null;
		}
		return dataSource.dot(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	public static final class ViewHolder {
		TextView coordXText;
		TextView coordYText;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.row,
					parent, false);
			holder = new ViewHolder();
			holder.coordXText = (TextView) convertView
					.findViewById(R.id.coordXText);
			holder.coordYText = (TextView) convertView
					.findViewById(R.id.coordYText);
			convertView.setTag(holder);
		}

		Dot dot = (Dot) getItem(position);
		holder = (ViewHolder) convertView.getTag();
		holder.coordXText.setText(dot.getX() + "");
		holder.coordYText.setText(dot.getY() + "");

		return convertView;
	}
}
