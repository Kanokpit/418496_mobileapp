package th.ac.ku.android.sutee.dotdot.fragment;

import android.os.Parcel;
import android.os.Parcelable;

public final class Dot implements Parcelable {
	public interface OnDotChangeListener {
		void onDotChange(Dot dot);
	}

	int x;
	int y;
	int color;
	int diameter;
	long id;

	OnDotChangeListener onDotChangeListener;

	public Dot(int x, int y, int color, int diameter, long id) {
		this.x = x;
		this.y = y;
		this.color = color;
		this.diameter = diameter;
		this.id = id;
	}

	public Dot(Parcel in) {
		readFromParcel(in);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
		notifyListener();
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
		notifyListener();
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
		notifyListener();
	}

	public int getDiameter() {
		return diameter;
	}

	public void setDiameter(int diameter) {
		this.diameter = diameter;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setOnDotChangeListener(OnDotChangeListener dotChangeListener) {
		this.onDotChangeListener = dotChangeListener;
	}

	public String toString() {
		return String.format("%d:%d:%d", this.x, this.y, this.diameter);
	}

	private void notifyListener() {
		if (onDotChangeListener != null) {
			onDotChangeListener.onDotChange(this);
		}
	}

	private void readFromParcel(Parcel in) {
		this.x = in.readInt();
		this.y = in.readInt();
		this.color = in.readInt();
		this.diameter = in.readInt();
		this.id = in.readLong();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.x);
		dest.writeInt(this.y);
		dest.writeInt(this.color);
		dest.writeInt(this.diameter);
		dest.writeLong(this.id);
	}

	@SuppressWarnings("rawtypes")
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

		@Override
		public Object createFromParcel(Parcel source) {
			return new Dot(source);
		}

		@Override
		public Object[] newArray(int size) {
			return new Dot[size];
		}

	};
}
