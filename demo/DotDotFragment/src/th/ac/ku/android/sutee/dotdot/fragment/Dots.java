package th.ac.ku.android.sutee.dotdot.fragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

public final class Dots implements Parcelable {

	public interface OnDotsChangeListener {
		void onDotsChange();
	}

	ArrayList<Dot> dots = new ArrayList<Dot>();
	OnDotsChangeListener onDotsChangeListener;

	public Dots() {
	};

	public Dots(Parcel in) {
		readFromParcel(in);
	}

	public void addDot(int x, int y, int color, int diameter) {
		this.dots.add(new Dot(x, y, color, diameter, UIDUtils.getNextUID()));
		notifyListener();
	}

	public void addDot(int x, int y, int color, int diameter, long id) {
		this.dots.add(new Dot(x, y, color, diameter, id));
		notifyListener();
	}

	public void removeDot(Dot dot) {
		this.dots.remove(dot);
		notifyListener();
	}

	public void removeDots(ArrayList<Dot> dots) {
		for (Dot dot : dots) {
			this.dots.remove(dot);
		}
		notifyListener();
	}

	public List<Dot> getDots() {
		return Collections.unmodifiableList(this.dots);
	}

	public void setDots(List<Dot> dots) {
		this.dots.clear();
		for (Dot dot : dots) {
			this.dots.add(dot);
		}
		notifyListener();
	}

	public Dot getDot(int position) {
		return dots.get(position);
	}

	public int size() {
		return dots.size();
	}

	public void clearDots() {
		this.dots.clear();
		notifyListener();
	}

	public void setOnDotsChangeListener(
			OnDotsChangeListener onDotsChangeListener) {
		this.onDotsChangeListener = onDotsChangeListener;
	}

	private void notifyListener() {
		if (this.onDotsChangeListener != null) {
			this.onDotsChangeListener.onDotsChange();
		}
	}

	private void readFromParcel(Parcel in) {
		in.readList(this.dots, null);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeList(this.dots);
	}

	@SuppressWarnings("rawtypes")
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		@Override
		public Dots createFromParcel(Parcel source) {
			return new Dots(source);
		}

		@Override
		public Dots[] newArray(int size) {
			return new Dots[size];
		}
	};
}
