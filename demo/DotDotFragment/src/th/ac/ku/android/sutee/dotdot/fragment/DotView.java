package th.ac.ku.android.sutee.dotdot.fragment;

import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class DotView extends View implements OnTouchListener {

	private DotDataSource dataSource;
	private OnDotTouchListener onDotTouchListener;

	public interface DotDataSource {
		List<Dot> dots(DotView view);
	}

	public interface OnDotTouchListener {
		void onDotTouch(int x, int y, float pressure, float size);
	}

	public DotView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setFocusable(true);
		setOnTouchListener(this);
	}

	public DotView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setFocusable(true);
		setOnTouchListener(this);
	}

	public DotView(Context context) {
		super(context);
		setFocusable(true);
		setOnTouchListener(this);
	}

	private void notifyDotTouch(int x, int y, float pressure, float size) {
		if (this.onDotTouchListener != null) {
			this.onDotTouchListener.onDotTouch(x, y, pressure, size);
		}
	}

	public void setDataSource(DotDataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void setOnDotTouchListener(OnDotTouchListener onDotTouchListener) {
		this.onDotTouchListener = onDotTouchListener;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		Paint paint = new Paint();
		canvas.drawColor(Color.WHITE);
		paint.setStyle(Style.STROKE);
		paint.setStrokeWidth(5);
		paint.setColor(hasFocus() ? Color.GREEN : Color.GRAY);
		canvas.drawRect(0, 0, getWidth() - 1, getHeight() - 1, paint);

		if (this.dataSource != null && this.dataSource.dots(this) != null) {
			paint.setStyle(Style.FILL);
			for (Dot dot : this.dataSource.dots(this)) {
				paint.setColor(dot.color);
				canvas.drawCircle(dot.x, dot.y, dot.diameter, paint);
			}
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getActionMasked()) {
		case MotionEvent.ACTION_DOWN:
			this.notifyDotTouch((int) event.getX(), (int) event.getY(),
					event.getPressure(), event.getSize());
			return true;
		}
		return false;
	}
}
