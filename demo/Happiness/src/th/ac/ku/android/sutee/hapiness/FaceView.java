package th.ac.ku.android.sutee.hapiness;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.OnScaleGestureListener;
import android.view.View;

public class FaceView extends View implements OnScaleGestureListener {

	public static final float DEFAULT_SCALE = 0.8f;
	private float scale;
	
//	private int happiness;
	
	public interface FaceViewDataSource {
		float smilingFactor(FaceView faceView);
	}
	
	private FaceViewDataSource dataSource;
		
	public void setDataSource(FaceViewDataSource dataSource) {
		this.dataSource = dataSource;
	}

//	public int getHappiness() {
//		return happiness;
//	}
//
//	public void setHappiness(int happiness) {
//		if (happiness != this.happiness) {
//			this.happiness = happiness;
//			this.invalidate();
//		}
//	}

	public float getScale() {
		return (scale == 0) ? DEFAULT_SCALE : scale;
	}

	public void setScale(float scale) {
		if (this.scale != scale) {
			this.scale = scale;
			this.invalidate();
		}
	}

	public FaceView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public FaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FaceView(Context context) {
		super(context);
	}

	private void drawCircle(PointF point, float radius, Canvas canvas,
			Paint paint) {
		canvas.save();
		canvas.drawCircle(point.x, point.y, radius, paint);
		canvas.restore();
	}

	private void drawCurve(PointF startPoint, PointF endPoint,
			PointF controlPoint, Canvas canvas, Paint paint) {
		canvas.save();
		Path path = new Path();
		path.moveTo(startPoint.x, startPoint.y);
		path.quadTo(controlPoint.x, controlPoint.y, endPoint.x, endPoint.y);
		canvas.drawPath(path, paint);
		canvas.restore();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		Rect rect = canvas.getClipBounds();
		PointF midPoint = new PointF((rect.right - rect.left) / 2,
				(rect.bottom - rect.top) / 2);
		int size = (getWidth() > getHeight()) ? getHeight() / 2
				: getWidth() / 2;
		size *= getScale();

		Paint paint = new Paint();
		paint.setColor(Color.BLUE);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(5);

		drawCircle(midPoint, size, canvas, paint);
		drawCircle(new PointF(midPoint.x - size / 2, midPoint.y - size / 2),
				size / 10, canvas, paint);
		drawCircle(new PointF(midPoint.x + size / 2, midPoint.y - size / 2),
				size / 10, canvas, paint);

		
//		float smilingFactor = -1 + 0.02f * getHappiness();
		float smilingFactor = dataSource.smilingFactor(this);
		
		drawCurve(new PointF(midPoint.x - size / 2, midPoint.y + size / 2),
				new PointF(midPoint.x + size / 2, midPoint.y + size / 2),
				new PointF(midPoint.x, midPoint.y + size / 2 + smilingFactor
						* size / 2), canvas, paint);
	}

	@Override
	public boolean onScale(ScaleGestureDetector detector) {
		setScale(getScale() * detector.getScaleFactor());
		return true;
	}

	@Override
	public boolean onScaleBegin(ScaleGestureDetector detector) {
		return true;
	}

	@Override
	public void onScaleEnd(ScaleGestureDetector detector) {
	}

}
