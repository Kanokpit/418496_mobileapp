package th.ac.ku.android.sutee.dotdot.rest;

import org.json.JSONArray;
import org.json.JSONException;

import android.os.Handler;

public abstract class JSONResponseHandler {

	private Handler mHandler = new Handler();

	public abstract void onProcess(JSONArray jsonArray);

	public void process(final String response) {
		if (response != null) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						final JSONArray jsonArray = new JSONArray(response);
						mHandler.post(new Runnable() {
							@Override
							public void run() {
								onProcess(jsonArray);
							}
						});
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}).run();

		}
	}
}