package th.ac.ku.android.sutee.dotdot.rest;

import java.sql.SQLException;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import com.j256.ormlite.android.AndroidCompiledStatement;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.StatementBuilder.StatementType;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

public class DotProvider extends ContentProvider {
	private static final int DOTS = 1;
	private static final int DOT_ID = 2;
	private static final UriMatcher sUriMatcher;
	private static final String BASE_PATH = "dots";

	public static final String AUTHORITY = "th.ac.ku.android.sutee.dotdot.rest";
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + BASE_PATH);

	public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.dotdot.dots";
	public static final String CONTENT_DOT_TYPE = "vnd.android.cursor.item/vnd.dotdot.dots";

	static {
		sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		sUriMatcher.addURI(AUTHORITY, BASE_PATH, DOTS);
		sUriMatcher.addURI(AUTHORITY, BASE_PATH + "/#", DOT_ID);
	}

	private DatabaseHelper databaseHelper;

	private DatabaseHelper getHelper() {
		if (databaseHelper == null) {
			databaseHelper = OpenHelperManager.getHelper(getContext(),
					DatabaseHelper.class);
		}
		return databaseHelper;
	}

	private SelectArg[] convertSeletionArgs(String[] args) {
		SelectArg[] selectArgs = new SelectArg[0];
		if (args != null) {
			selectArgs = new SelectArg[args.length];
			for (int index = 0; index < args.length; index++) {
				selectArgs[index] = new SelectArg(SqlType.STRING, args[index]);
			}
		}
		return selectArgs;
	}

	private Dot convertContentValues(ContentValues values) {
		int x = values.getAsInteger(Dot.X_COLUMN);
		int y = values.getAsInteger(Dot.Y_COLUMN);
		String gid = values.getAsString(Dot.GID_COLUMN);

		boolean statePosting = false;
		if (values.containsKey(Dot.STATE_POSTING_COLUMN)) {
			statePosting = values.getAsBoolean(Dot.STATE_POSTING_COLUMN);
		}

		boolean stateUpdating = false;
		if (values.containsKey(Dot.STATE_UPDATING_COLUMN)) {
			stateUpdating = values.getAsBoolean(Dot.STATE_UPDATING_COLUMN);
		}

		boolean stateDeleting = false;
		if (values.containsKey(Dot.STATE_DELETING_COLUMN)) {
			stateDeleting = values.getAsBoolean(Dot.STATE_DELETING_COLUMN);
		}
		return new Dot(x, y, gid, statePosting, stateUpdating, stateDeleting);
	}

	@Override
	public boolean onCreate() {
		if (getHelper() == null) {
			return false;
		}

		if (getHelper().getDotDao() == null) {
			return false;
		}

		return true;
	}

	@Override
	public String getType(Uri uri) {
		switch (sUriMatcher.match(uri)) {
		case DOTS:
			return CONTENT_TYPE;
		case DOT_ID:
			return CONTENT_DOT_TYPE;
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int count = 0;
		try {
			DeleteBuilder<Dot, Integer> dBuilder = getHelper().getDotDao()
					.deleteBuilder();
			Where<Dot, Integer> where = dBuilder.where();
			where.isNotNull(Dot.ID_COLUMN); // we need at least one where clause
			if (selection != null) {
				where.and().raw(selection, convertSeletionArgs(selectionArgs));
			}
			switch (sUriMatcher.match(uri)) {
			case DOTS:
				break;
			case DOT_ID:
				if (selection != null) {
					where.and();
				}
				where.idEq(Integer.parseInt(uri.getPathSegments().get(1)));
				break;
			default:
				throw new IllegalArgumentException("Unknown URI " + uri);
			}
			count = getHelper().getDotDao().delete(dBuilder.prepare());
			getContext().getContentResolver().notifyChange(uri, null);
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Dot dot = convertContentValues(values);
		try {
			if (getHelper().getDotDao().create(dot) == 1) {
				getContext().getContentResolver().notifyChange(uri, null);
				return ContentUris.withAppendedId(CONTENT_URI, dot.getId());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		throw new android.database.SQLException("Failed to insert row into "
				+ uri);
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		QueryBuilder<Dot, Integer> qBuilder = getHelper().getDotDao()
				.queryBuilder();
		Where<Dot, Integer> where = qBuilder.where();
		if (projection != null) {
			qBuilder.selectColumns(projection);
		}
		if (sortOrder != null) {
			qBuilder.orderBy(sortOrder, true);
		}
		Cursor cursor = null;
		try {
			where.isNotNull(Dot.ID_COLUMN); // we need at least one where clause
			if (selection != null) {
				where.and().raw(selection, convertSeletionArgs(selectionArgs));
			}
			switch (sUriMatcher.match(uri)) {
			case DOTS:
				break;
			case DOT_ID:
				if (selection != null) {
					where.and();
				}
				where.idEq(Integer.parseInt(uri.getPathSegments().get(1)));
				break;
			default:
				throw new IllegalArgumentException("Unknown URI " + uri);
			}
			PreparedQuery<Dot> query = qBuilder.prepare();
			AndroidCompiledStatement compiledStatement = (AndroidCompiledStatement) query
					.compile(getHelper().getConnectionSource()
							.getReadOnlyConnection(), StatementType.SELECT);
			cursor = compiledStatement.getCursor();
			cursor.setNotificationUri(getContext().getContentResolver(), uri);
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
		}
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int count = 0;
		try {
			UpdateBuilder<Dot, Integer> uBuilder = getHelper().getDotDao()
					.updateBuilder();

			if (values.containsKey(Dot.X_COLUMN)) {
				uBuilder.updateColumnValue(Dot.X_COLUMN,
						values.getAsInteger(Dot.X_COLUMN));
			}

			if (values.containsKey(Dot.Y_COLUMN)) {
				uBuilder.updateColumnValue(Dot.Y_COLUMN,
						values.getAsInteger(Dot.Y_COLUMN));
			}

			if (values.containsKey(Dot.STATE_POSTING_COLUMN)) {
				uBuilder.updateColumnValue(Dot.STATE_POSTING_COLUMN,
						values.getAsBoolean(Dot.STATE_POSTING_COLUMN));
			}

			if (values.containsKey(Dot.STATE_UPDATING_COLUMN)) {
				uBuilder.updateColumnValue(Dot.STATE_UPDATING_COLUMN,
						values.getAsBoolean(Dot.STATE_UPDATING_COLUMN));
			}

			if (values.containsKey(Dot.STATE_DELETING_COLUMN)) {
				uBuilder.updateColumnValue(Dot.STATE_DELETING_COLUMN,
						values.getAsBoolean(Dot.STATE_DELETING_COLUMN));
			}

			Where<Dot, Integer> where = uBuilder.where();
			where.isNotNull(Dot.ID_COLUMN); // we need at least one where clause
			if (selection != null) {
				where.and().raw(selection, convertSeletionArgs(selectionArgs));
			}
			switch (sUriMatcher.match(uri)) {
			case DOTS:
				break;
			case DOT_ID:
				if (selection != null) {
					where.and();
				}
				where.idEq(Integer.parseInt(uri.getPathSegments().get(1)));
				break;
			default:
				throw new IllegalArgumentException("Unknown URI " + uri);
			}
			count = getHelper().getDotDao().update(uBuilder.prepare());
			getContext().getContentResolver().notifyChange(uri, null);
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

}
