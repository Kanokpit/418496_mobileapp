from django.db import models

class Dot(models.Model):
    x = models.IntegerField()
    y = models.IntegerField()
    radius = models.FloatField()
    color = models.IntegerField()
    alpha = models.IntegerField()
    gid = models.TextField(unique=True)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated_at = models.DateTimeField(auto_now_add=True, auto_now=True)
    
    def __unicode__(self):
        return '%d,%d' % (self.x, self.y)
