//
//  Photo+Flickr.h
//  FlickrFetcher
//
//  Created by Sutee Sudprasert on 3/26/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Photo.h"

@interface Photo (Flickr)

+ (Photo *)photoWithFlickrInfo:(id)flickrInfo inManagedObjectContext:(NSManagedObjectContext *)context;

@end
