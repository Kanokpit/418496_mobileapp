//
//  Photographer+Create.m
//  FlickrFetcher
//
//  Created by Sutee Sudprasert on 3/26/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "Photographer+Create.h"

@implementation Photographer (Create)

+ (Photographer *)photographerWithUID:(NSString *)uid inManagedObjectContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photographer"];
    request.predicate = [NSPredicate predicateWithFormat:@"uid = %@", uid];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    
    Photographer *photographer = nil;    
    
    if (error || results.count > 1) {
        // handle error
        NSLog(@"%@", error.localizedDescription);
    } else if (results.count == 0) {
        photographer = [NSEntityDescription insertNewObjectForEntityForName:@"Photographer" inManagedObjectContext:context]; 
        photographer.uid = uid;
    } else {
        photographer = results.lastObject;
    }
    return photographer;    
}


@end
