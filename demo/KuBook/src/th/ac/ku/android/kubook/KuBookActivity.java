package th.ac.ku.android.kubook;

import th.ac.ku.android.kubook.KuBook.DatabaseHelper;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;

public class KuBookActivity extends FragmentActivity implements
		LoaderCallbacks<Cursor> {

	private static final int MENU_ADD = 1;
	private SimpleBookCursorAdapter mAdapter;
	private ListView listView;
	private DatabaseHelper databaseHelper;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		listView = (ListView) findViewById(R.id.listView);
		mAdapter = new SimpleBookCursorAdapter(this, null, 0);
		listView.setAdapter(mAdapter);
		getSupportLoaderManager().initLoader(0, null, this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (databaseHelper != null) {
			OpenHelperManager.releaseHelper();
			databaseHelper = null;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(Menu.NONE, MENU_ADD, Menu.NONE, R.string.add);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_ADD:
			showAddDialog();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void showAddDialog() {
		DialogFragment fragment = new AddDialogFragment();
		fragment.show(getSupportFragmentManager(), "dialog");
	}

	public void addBook(String title, String author, String isbn, float price) {
		ContentValues values = new ContentValues();
		values.put(KuBook.Book.TITLE_COLUMN, title);
		values.put(KuBook.Book.AUTHOR_COLUMN, author);
		values.put(KuBook.Book.ISBN_COLUMN, isbn);
		values.put(KuBook.Book.PRICE_COLUMN, new Float(price));
		getContentResolver().insert(KuBook.SimpleBooks.CONTENT_URI, values);
	}

	@SuppressWarnings("unused")
	private DatabaseHelper getHelper() {
		if (databaseHelper == null) {
			databaseHelper = OpenHelperManager.getHelper(this,
					DatabaseHelper.class);
		}
		return databaseHelper;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
		return new CursorLoader(this, KuBook.SimpleBooks.CONTENT_URI, null,
				null, null, KuBook.Book.TITLE_COLUMN);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		mAdapter.swapCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
	}

	public static class SimpleBookCursorAdapter extends CursorAdapter {
		Context mContext;

		public SimpleBookCursorAdapter(Context context, Cursor c, int flags) {
			super(context, c, flags);
			mContext = context;
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			ViewHolder viewHolder = new ViewHolder();
			View row = LayoutInflater.from(mContext).inflate(R.layout.row,
					parent, false);
			viewHolder.titleText = (TextView) row.findViewById(R.id.titleText);
			viewHolder.authorText = (TextView) row
					.findViewById(R.id.authorText);
			viewHolder.priceText = (TextView) row.findViewById(R.id.priceText);
			viewHolder.isbnText = (TextView) row.findViewById(R.id.isbnText);
			row.setTag(viewHolder);
			return row;
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			ViewHolder viewHolder = (ViewHolder) view.getTag();
			String title = cursor.getString(cursor
					.getColumnIndex(KuBook.Book.TITLE_COLUMN));
			String author = cursor.getString(cursor
					.getColumnIndex(KuBook.Book.AUTHOR_COLUMN));
			String isbn = cursor.getString(cursor
					.getColumnIndex(KuBook.Book.ISBN_COLUMN));
			float price = cursor.getFloat(cursor
					.getColumnIndex(KuBook.Book.PRICE_COLUMN));
			viewHolder.titleText.setText(title);
			viewHolder.authorText.setText(author);
			viewHolder.isbnText.setText("ISBN:" + isbn);
			viewHolder.priceText.setText(String.format("%.2f", price));
		}

		public static final class ViewHolder {
			TextView titleText;
			TextView authorText;
			TextView priceText;
			TextView isbnText;
		}
	}

	public static class AddDialogFragment extends DialogFragment {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final View view = getActivity().getLayoutInflater().inflate(
					R.layout.add_dialog, null);
			return new AlertDialog.Builder(getActivity())
					.setTitle("Add a new book").setView(view)
					.setPositiveButton("OK", new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							String title = ((EditText) view
									.findViewById(R.id.titleField)).getText()
									.toString();
							String author = ((EditText) view
									.findViewById(R.id.authorField)).getText()
									.toString();
							String isbn = ((EditText) view
									.findViewById(R.id.isbnField)).getText()
									.toString();
							float price = Float.parseFloat(((EditText) view
									.findViewById(R.id.priceField)).getText()
									.toString());
							((KuBookActivity) getActivity()).addBook(title,
									author, isbn, price);
						}
					}).setNegativeButton("Cancel", new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					}).create();
		}

	}
}