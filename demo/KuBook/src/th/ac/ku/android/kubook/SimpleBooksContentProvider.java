package th.ac.ku.android.kubook;

import th.ac.ku.android.kubook.KuBook.Book;
import th.ac.ku.android.kubook.KuBook.DatabaseHelper;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import com.j256.ormlite.android.AndroidCompiledStatement;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.StatementBuilder.StatementType;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

public class SimpleBooksContentProvider extends ContentProvider {
	private static final int BOOKS = 1;
	private static final int BOOK_ID = 2;
	private static final UriMatcher sUriMatcher;

	static {
		sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		sUriMatcher.addURI(KuBook.SimpleBooks.AUTHORITY,
				KuBook.SimpleBooks.BOOKS, BOOKS);
		sUriMatcher.addURI(KuBook.SimpleBooks.AUTHORITY,
				KuBook.SimpleBooks.BOOKS + "/#", BOOK_ID);
	}

	private DatabaseHelper databaseHelper;
	private Context mContext;
	private RuntimeExceptionDao<KuBook.Book, Integer> bookDao;

	@Override
	public String getType(Uri uri) {
		switch (sUriMatcher.match(uri)) {
		// ---get all items---
		case BOOKS:
			return KuBook.SimpleBooks.CONTENT_TYPE;
			// ---get a particular item---
		case BOOK_ID:
			return KuBook.SimpleBooks.CONTENT_BOOK_TYPE;
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}

	@Override
	public boolean onCreate() {
		mContext = getContext();
		if (getHelper() == null) {
			return false;
		}
		try {
			bookDao = getHelper().getBookDao();
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int count = 0;
		try {
			DeleteBuilder<KuBook.Book, Integer> dBuilder = bookDao
					.deleteBuilder();
			Where<KuBook.Book, Integer> where = dBuilder.where();
			where.isNotNull(KuBook.Book.ID_COLUMN); // dummy where clause
			if (selection != null) {
				where.and().raw(selection, convertSeletionArgs(selectionArgs));
			}
			switch (sUriMatcher.match(uri)) {
			case BOOKS:
				break;
			case BOOK_ID:
				if (selection != null) {
					where.and();
				}
				where.idEq(Integer.parseInt(uri.getPathSegments().get(1)));
				break;
			default:
				throw new IllegalArgumentException("Unknown URI " + uri);
			}
			count = bookDao.delete(dBuilder.prepare());
			mContext.getContentResolver().notifyChange(uri, null);
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Book book = convertContentValues(values);
		if (bookDao.create(book) == 1) {
			mContext.getContentResolver().notifyChange(uri, null);
			return ContentUris.withAppendedId(KuBook.SimpleBooks.CONTENT_URI,
					book.getId());
		}

		throw new android.database.SQLException("Failed to insert row into "
				+ uri);
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		QueryBuilder<KuBook.Book, Integer> qBuilder = bookDao.queryBuilder();
		Where<KuBook.Book, Integer> where = qBuilder.where();
		if (projection != null) {
			qBuilder.selectColumns(projection);
		}
		if (sortOrder != null) {
			qBuilder.orderBy(sortOrder, true);
		}
		Cursor cursor = null;
		try {
			where.isNotNull(KuBook.Book.ID_COLUMN); // dummy where clause
			if (selection != null) {
				where.and().raw(selection, convertSeletionArgs(selectionArgs));
			}
			switch (sUriMatcher.match(uri)) {
			case BOOKS:
				break;
			case BOOK_ID:
				if (selection != null) {
					where.and();
				}
				where.idEq(Integer.parseInt(uri.getPathSegments().get(1)));
				break;
			default:
				throw new IllegalArgumentException("Unknown URI " + uri);
			}
			PreparedQuery<KuBook.Book> query = qBuilder.prepare();
			AndroidCompiledStatement compiledStatement = (AndroidCompiledStatement) query
					.compile(getHelper().getConnectionSource()
							.getReadOnlyConnection(), StatementType.SELECT);
			cursor = compiledStatement.getCursor();
			cursor.setNotificationUri(mContext.getContentResolver(), uri);
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
		}
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int count = 0;
		try {
			UpdateBuilder<KuBook.Book, Integer> uBuilder = bookDao
					.updateBuilder();
			
			if (values.containsKey(KuBook.Book.ISBN_COLUMN)) {
				uBuilder.updateColumnValue(KuBook.Book.ISBN_COLUMN,
						values.get(KuBook.Book.ISBN_COLUMN));
			}
			
			if (values.containsKey(KuBook.Book.AUTHOR_COLUMN)) {
				uBuilder.updateColumnValue(KuBook.Book.AUTHOR_COLUMN,
						values.get(KuBook.Book.AUTHOR_COLUMN));
			}
			
			if (values.containsKey(KuBook.Book.TITLE_COLUMN)) {
				uBuilder.updateColumnValue(KuBook.Book.TITLE_COLUMN,
						values.get(KuBook.Book.TITLE_COLUMN));
			}

			if (values.containsKey(KuBook.Book.PRICE_COLUMN)) {
				uBuilder.updateColumnValue(KuBook.Book.PRICE_COLUMN,
						values.get(KuBook.Book.PRICE_COLUMN));
			}
			
			Where<KuBook.Book, Integer> where = uBuilder.where();
			where.isNotNull(KuBook.Book.ID_COLUMN); // dummy where clause
			if (selection != null) {
				where.and().raw(selection, convertSeletionArgs(selectionArgs));
			}
			switch (sUriMatcher.match(uri)) {
			case BOOKS:
				break;
			case BOOK_ID:
				if (selection != null) {
					where.and();
				}
				where.idEq(Integer.parseInt(uri.getPathSegments().get(1)));
				break;
			default:
				throw new IllegalArgumentException("Unknown URI " + uri);
			}
			count = bookDao.update(uBuilder.prepare());
			mContext.getContentResolver().notifyChange(uri, null);
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

	private SelectArg[] convertSeletionArgs(String[] args) {
		SelectArg[] selectArgs = new SelectArg[0];
		if (args != null) {
			selectArgs = new SelectArg[args.length];
			for (int index = 0; index < args.length; index++) {
				selectArgs[index] = new SelectArg(SqlType.STRING, args[index]);
			}
		}
		return selectArgs;
	}

	private KuBook.Book convertContentValues(ContentValues values) {
		String title = values.getAsString(KuBook.Book.TITLE_COLUMN);
		Float price = values.getAsFloat(KuBook.Book.PRICE_COLUMN);
		String author = values.getAsString(KuBook.Book.AUTHOR_COLUMN);
		String isbn = values.getAsString(KuBook.Book.ISBN_COLUMN);
		return new KuBook.Book(title, price.floatValue(), author, isbn);
	}

	private DatabaseHelper getHelper() {
		if (databaseHelper == null) {
			databaseHelper = OpenHelperManager.getHelper(mContext,
					DatabaseHelper.class);
		}
		return databaseHelper;
	}

}
