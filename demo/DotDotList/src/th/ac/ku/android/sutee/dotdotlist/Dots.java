package th.ac.ku.android.sutee.dotdotlist;

import java.util.ArrayList;

public class Dots {

	public static class Dot {
		private int x;
		private int y;
		
		public Dot(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public int getX() {
			return x;
		}

		public void setX(int x) {
			this.x = x;
		}

		public int getY() {
			return y;
		}

		public void setY(int y) {
			this.y = y;
		}
	}

	public static interface OnDotsChangeListener {
		void onDotsChange();
	}

	private ArrayList<Dot> dots;
	private OnDotsChangeListener mListener;

	public Dots() {
		this.dots = new ArrayList<Dots.Dot>();
	}

	public void add(Dot dot) {
		this.dots.add(dot);
		this.notifyOnDotsChange();
	}
	
	public int size() {
		return this.dots.size();
	}

	public Dot get(int position) {
		return this.dots.get(position);
	}
	
	public void setOnDotsChangeListener(OnDotsChangeListener mListener) {
		this.mListener = mListener;
	}

	private void notifyOnDotsChange() {
		if (this.mListener != null) {
			this.mListener.onDotsChange();
		}
	}

}
