//
//  Dots.h
//  DotDot
//
//  Created by Sutee Sudprasert on 2/27/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Dot.h"

@protocol DotsDelegate <NSObject>

- (void)dotsDidChange:(NSArray *)dots;

@end

@interface Dots : NSObject

@property (nonatomic, weak) id<DotsDelegate> delegate;
@property (nonatomic, strong, readonly) NSArray *dotItems;

- (void)addDot:(Dot *)dot;

@end
