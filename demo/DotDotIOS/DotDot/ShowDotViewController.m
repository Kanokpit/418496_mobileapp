//
//  ShowDotViewController.m
//  DotDot
//
//  Created by Sutee Sudprasert on 3/5/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "ShowDotViewController.h"
#import "DotDotViewController.h"

@interface ShowDotViewController()

@property (nonatomic, strong) UIColor *dotColor;

@end

@implementation ShowDotViewController

@synthesize dotColor = _dotColor;

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowDotView"]) {
         DotDotViewController *controller = segue.destinationViewController;
        [controller setDotColor:self.dotColor];
    }
}

#pragma mark - View lifecycle

- (IBAction)showRedDotAction:(id)sender {
    self.dotColor = [UIColor redColor];
    [self performSegueWithIdentifier:@"ShowDotView" sender:self];
}

- (IBAction)showBlueDotAction:(id)sender {
    self.dotColor = [UIColor blueColor];    
    [self performSegueWithIdentifier:@"ShowDotView" sender:self];
}

- (IBAction)showGreenDotAction:(id)sender {
    self.dotColor = [UIColor greenColor];    
    [self performSegueWithIdentifier:@"ShowDotView" sender:self];    
}

@end
