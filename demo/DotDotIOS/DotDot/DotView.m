//
//  DotView.m
//  DotDot
//
//  Created by Sutee Sudprasert on 2/27/55 BE.
//  Copyright (c) 2555 __MyCompanyName__. All rights reserved.
//

#import "DotView.h"
#import "Dot.h"

@implementation DotView

@synthesize dataSource = _dataSource;
@synthesize dotColor = _dotColor;

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.dotColor set];
    for (Dot *dot in [self.dataSource dotItems]) {

        NSLog(@"%f %f", dot.origin.x, dot.origin.y);
        CGContextFillEllipseInRect(context, CGRectMake(dot.origin.x, dot.origin.y, 10, 10));
    }
}

@end
